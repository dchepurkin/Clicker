// Created by DChepurkin, All right reserved

#include "Actors/ClickerScoreFarmBuilding.h"

#include "ClickerCharacter.h"

void AClickerScoreFarmBuilding::Activate(const FBuildingInfo& Info)
{
	Super::Activate(Info);
	GetWorldTimerManager().SetTimer(FarmTimerHandle, this, &ThisClass::FarmTimerCallback, FarmRate, true);
}

void AClickerScoreFarmBuilding::UpdateScorePerTick()
{
	const auto ClickerCharacter = Cast<AClickerCharacter>(GetOwner());
	if(!ClickerCharacter) return;;

	ClickerCharacter->UseScore(GetScoreUpdatePrise());

	ScorePerTick += ScorePerTickUpdateCoef;
	++ScorePerTickCurrentUpdate;
}

int32 AClickerScoreFarmBuilding::GetScoreUpdatePrise() const
{
	if(!ScoreUpdateInfoDataTable) return 0;

	TArray<FUpdatePriseInfo*> Rows;
	ScoreUpdateInfoDataTable->GetAllRows("", Rows);

	const auto FindedRow = Rows.FindByPredicate([&](const FUpdatePriseInfo* Row)
	{
		return Row->UpdateLevel == ScorePerTickCurrentUpdate;
	});

	return FindedRow ? (*FindedRow)->UpdatePrise : 0;
}

void AClickerScoreFarmBuilding::UpdateFarmRate()
{
	const auto ClickerCharacter = Cast<AClickerCharacter>(GetOwner());
	if(!ClickerCharacter) return;;

	ClickerCharacter->UseScore(GetFarmRateUpdatePrise());

	FarmRate -= FarmRateUpdateCoef;
	++FarmRateCurrentUpdate;
	GetWorldTimerManager().SetTimer(FarmTimerHandle, this, &ThisClass::FarmTimerCallback, FarmRate, true);
}

int32 AClickerScoreFarmBuilding::GetFarmRateUpdatePrise() const
{
	if(!FarmRateUpdateInfoDataTable) return 0;

	TArray<FUpdatePriseInfo*> Rows;
	FarmRateUpdateInfoDataTable->GetAllRows("", Rows);

	const auto FindedRow = Rows.FindByPredicate([&](const FUpdatePriseInfo* Row)
	{
		return Row->UpdateLevel == FarmRateCurrentUpdate;
	});

	return FindedRow ? (*FindedRow)->UpdatePrise : 0;
}

void AClickerScoreFarmBuilding::FarmTimerCallback() const
{
	const auto ClickerCharacter = Cast<AClickerCharacter>(GetOwner());
	if(!ClickerCharacter) return;;

	ClickerCharacter->AddScore(ScorePerTick);
}
