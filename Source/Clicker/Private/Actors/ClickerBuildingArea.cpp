// Created by DChepurkin, All right reserved

#include "Actors/ClickerBuildingArea.h"

#include "Components/BoxComponent.h"
#include "Components/DecalComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMaterialLibrary.h"

DEFINE_LOG_CATEGORY_STATIC(LogBuildArea, All, All);

AClickerBuildingArea::AClickerBuildingArea()
{
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<UBoxComponent>("Collision");
	Collision->SetCollisionResponseToAllChannels(ECR_Overlap);
	SetRootComponent(Collision);
}

void AClickerBuildingArea::BeginPlay()
{
	Super::BeginPlay();

	if(DecalMaterial)
	{
		DecalMaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(this, DecalMaterial);
	}

	if(Collision)
	{
		Collision->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::BeginOverlapCallback);
		Collision->OnComponentEndOverlap.AddDynamic(this, &ThisClass::EndOverlapCallback);
	}

	constexpr float SizeZ = 250.f;
	const FVector AreaSize = FVector(BuildingInfo.Size, SizeZ);
	Collision->SetBoxExtent(AreaSize);
	Decal = UGameplayStatics::SpawnDecalAttached(DecalMaterialInstance, AreaSize, Collision);
}

void AClickerBuildingArea::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(!OwningController) return;

	FHitResult HitResult;
	OwningController->GetHitResultUnderCursorByChannel(UEngineTypes::ConvertToTraceType(ECC_WorldStatic), false, HitResult);
	if(Decal) Decal->SetVisibility(HitResult.bBlockingHit);
	SetActorLocation(HitResult.ImpactPoint);
}

void AClickerBuildingArea::Init(const FBuildingInfo& Info, APlayerController* Controller)
{
	BuildingInfo = Info;
	OwningController = Controller;
}

void AClickerBuildingArea::BeginOverlapCallback(UPrimitiveComponent* OverlappedComponent,
                                                AActor* OtherActor,
                                                UPrimitiveComponent* OtherComp,
                                                int32 OtherBodyIndex,
                                                bool bFromSweep,
                                                const FHitResult& SweepResult)
{
	bCanBuild = false;
	UpdateColor();
}

void AClickerBuildingArea::EndOverlapCallback(UPrimitiveComponent* OverlappedComponent,
                                              AActor* OtherActor,
                                              UPrimitiveComponent* OtherComp,
                                              int32 OtherBodyIndex)
{
	TArray<AActor*> OverlappingActors;
	Collision->GetOverlappingActors(OverlappingActors);

	if(OverlappingActors.IsEmpty())
	{
		bCanBuild = true;
		UpdateColor();
	}
}

void AClickerBuildingArea::UpdateColor()
{
	DecalMaterialInstance->SetVectorParameterValue(ColorParameterName, FVector(CanBuildColor[bCanBuild]));
}
