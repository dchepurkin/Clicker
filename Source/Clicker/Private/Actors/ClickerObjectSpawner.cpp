// Created by DChepurkin, All right reserved

#include "Actors/ClickerObjectSpawner.h"

#include "ClickerObjectBase.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"

DEFINE_LOG_CATEGORY_STATIC(LogObjectSpawner, All, All);

AClickerObjectSpawner::AClickerObjectSpawner()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	SpawnArea = CreateDefaultSubobject<UBoxComponent>("SpawnArea");
	SpawnArea->SetCollisionResponseToAllChannels(ECR_Ignore);
	SetRootComponent(SpawnArea);
}

void AClickerObjectSpawner::BeginPlay()
{
	Super::BeginPlay();

	if(ObjectsClasses.Num() == 0)
	{
		//������� � ��� ������ ���� � ������ ObjectsClasses ������ � ������� �� �������
		UE_LOG(LogObjectSpawner, Error, TEXT("ObjectsClasses must not be empty"));
		return;
	}
	SpawnTimerCallback();
	GetWorldTimerManager().SetTimer(SpawnTimerHandle, this, &ThisClass::SpawnTimerCallback, SpawnRate, true);
}

void AClickerObjectSpawner::SpawnTimerCallback()
{
	if(CurrentObjectsCount >= MaxObjectsInScene || !GetWorld()) return;

	const auto SpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(GetActorLocation(), SpawnArea->Bounds.BoxExtent);

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

	const auto NewObject = GetWorld()->SpawnActor<AClickerObjectBase>(GetRandomObjectClass(), SpawnLocation, FRotator::ZeroRotator, SpawnParameters);

	if(NewObject)
	{
		++CurrentObjectsCount;
		NewObject->OnDestroy.AddUObject(this, &ThisClass::OnObjectDestroyCallback);
	}
}

const TSubclassOf<AClickerObjectBase>& AClickerObjectSpawner::GetRandomObjectClass() const
{
	const auto RandomIndex = FMath::RandHelper(ObjectsClasses.Num());
	return ObjectsClasses[RandomIndex];
}

void AClickerObjectSpawner::OnObjectDestroyCallback()
{
	--CurrentObjectsCount;
}
