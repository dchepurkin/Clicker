// Created by DChepurkin, All right reserved

#include "Actors/ClickerObjectBase.h"

#include "ClickerCharacter.h"
#include "Components/BoxComponent.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogClickerObject, All, All);

AClickerObjectBase::AClickerObjectBase()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	Collision = CreateDefaultSubobject<UBoxComponent>("Collision");
	Collision->SetCollisionResponseToAllChannels(ECR_Block);
	SetRootComponent(Collision);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	StaticMesh->bReceivesDecals = false;
	StaticMesh->SetupAttachment(Collision);

	ScoreProgressBar = CreateDefaultSubobject<UWidgetComponent>("ScoreProgressBar");
	ScoreProgressBar->SetupAttachment(Collision);
	ScoreProgressBar->SetVisibility(false);

	CurrentScore = MaxScore;
}

void AClickerObjectBase::OnClick(AClickerCharacter* ClickedCharacter)
{
	if(!ClickedCharacter) return;

	UGameplayStatics::PlaySound2D(this, ClickSound);
	UGameplayStatics::SpawnEmitterAtLocation(this, ClickParticle, GetActorLocation(), FRotator::ZeroRotator);
	
	const auto Damage = FMath::Clamp(ClickedCharacter->GetDamage(), 1, CurrentScore);
	CurrentScore -= Damage;
	ClickedCharacter->Mine(Damage);
	DamageEvent(Damage);

	if(CurrentScore == 0)
	{
		OnDestroy.Broadcast();
		Destroy();
	}
}

void AClickerObjectBase::HoverOn()
{
	if(!StaticMesh || !ScoreProgressBar) return;
	StaticMesh->SetRenderCustomDepth(true);
	ScoreProgressBar->SetVisibility(true);
}

void AClickerObjectBase::HoverOff()
{
	if(!StaticMesh || !ScoreProgressBar) return;
	StaticMesh->SetRenderCustomDepth(false);
	ScoreProgressBar->SetVisibility(false);
}