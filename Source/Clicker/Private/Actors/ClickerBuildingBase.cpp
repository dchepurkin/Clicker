// Created by DChepurkin, All right reserved

#include "Actors/ClickerBuildingBase.h"

#include "ClickerCharacter.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "UI/ClickerUpdateBuildingBase.h"

AClickerBuildingBase::AClickerBuildingBase()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetCollisionResponseToAllChannels(ECR_Block);
	SetRootComponent(StaticMesh);
}

void AClickerBuildingBase::BeginPlay()
{
	Super::BeginPlay();

	//������� ������ �������������� ��� ��������� ���������
	UpdateWidget = CreateWidget<UClickerUpdateBuildingBase>(GetWorld(), BuildingInfo.UpdateWidgetClass);
	if(UpdateWidget)
	{
		UpdateWidget->SetBuilding(this);
		UpdateWidget->SetVisibility(ESlateVisibility::Hidden);
		UpdateWidget->AddToViewport();
	}
}

void AClickerBuildingBase::Activate(const FBuildingInfo& Info)
{
	BuildingInfo = Info;
}

void AClickerBuildingBase::OnClick(AClickerCharacter* ClickedCharacter)
{
	if(!ClickedCharacter) return;

	ClickedCharacter->OpenBuildingWidget(UpdateWidget);
}

void AClickerBuildingBase::HoverOn()
{
	if(!StaticMesh) return;
	StaticMesh->SetRenderCustomDepth(true);
}

void AClickerBuildingBase::HoverOff()
{
	if(!StaticMesh) return;
	StaticMesh->SetRenderCustomDepth(false);
}
