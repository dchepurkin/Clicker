// Created by DChepurkin, All right reserved

#include "Player/ClickerStatsComponent.h"

UClickerStatsComponent::UClickerStatsComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void UClickerStatsComponent::BeginPlay()
{
	Super::BeginPlay();
}

int32 UClickerStatsComponent::GetDamage() const
{
	return FMath::RandRange(0.f, 1.f) <= DoubleDamageChance ? BaseDamage * 2 : BaseDamage;
}

void UClickerStatsComponent::AddDoubleDamageChance(const float AddedChance)
{
	DoubleDamageChance += AddedChance;
}
