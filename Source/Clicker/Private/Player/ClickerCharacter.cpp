// Created by DChepurkin, All right reserved

#include "Player/ClickerCharacter.h"

#include "Clickable.h"
#include "ClickerBuildingArea.h"
#include "ClickerBuildingBase.h"
#include "ClickerHUD.h"
#include "ClickerPlayerState.h"
#include "ClickerStatsComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

DEFINE_LOG_CATEGORY_STATIC(LogClickerCharacter, All, All);

AClickerCharacter::AClickerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationYaw = false;

	StatsComponent = CreateDefaultSubobject<UClickerStatsComponent>("StatsComponent");
}

void AClickerCharacter::BeginPlay()
{
	Super::BeginPlay();
	PlayerControllerCache = GetController<APlayerController>();
}

void AClickerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	CursorTick(DeltaSeconds);
}

void AClickerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if(!PlayerInputComponent) return;

	PlayerInputComponent->BindAxis("MoveForward", this, &ThisClass::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ThisClass::MoveRight);

	PlayerInputComponent->BindAction("Click", IE_Released, this, &ThisClass::Click);
	PlayerInputComponent->BindAction("Factory", IE_Pressed, this, &ThisClass::OpenCloseFactory);
	PlayerInputComponent->BindAction("Esc", IE_Pressed, this, &ThisClass::EscapeButton);
}

void AClickerCharacter::MoveForward(float AxisValue)
{
	if(FMath::IsNearlyZero(AxisValue)) return;

	const FVector Direction{1.f, 0.f, 0.f};
	AddMovementInput(Direction, AxisValue);
}

void AClickerCharacter::MoveRight(float AxisValue)
{
	if(FMath::IsNearlyZero(AxisValue)) return;

	const FVector Direction{0.f, 1.f, 0.f};
	AddMovementInput(Direction, AxisValue);
}

void AClickerCharacter::RotationToCursor(float DeltaSeconds)
{
	FVector MousePosition;
	PlayerControllerCache->GetMousePosition(MousePosition.X, MousePosition.Y);

	FVector2D CharacterScreenLocation2D;
	PlayerControllerCache->ProjectWorldLocationToScreen(GetActorLocation(), CharacterScreenLocation2D);
	const auto CharacterScreenLocation = FVector(CharacterScreenLocation2D, 0.f);

	auto TargetYawRotation = UKismetMathLibrary::FindLookAtRotation(CharacterScreenLocation, MousePosition);

	TargetYawRotation.Yaw -= GetMesh()->GetRelativeRotation().Yaw;
	const auto NewYawRotation = FMath::RInterpConstantTo(GetActorRotation(), TargetYawRotation, DeltaSeconds, RotationToCursorSpeed);
	SetActorRotation(NewYawRotation);
}

void AClickerCharacter::Click()
{
	if(CurrentState == EClickerState::Mine)
	{
		//�������� ���� ���������
		if(!PlayerControllerCache) return;

		FHitResult HitResult;
		PlayerControllerCache->GetHitResultUnderCursorByChannel(UEngineTypes::ConvertToTraceType(ECC_Visibility), false, HitResult);
		if(!HitResult.bBlockingHit) return;

		if(const auto ClickableIntarface = Cast<IClickable>(HitResult.GetActor()))
		{
			if(FVector::Distance(GetActorLocation(), HitResult.GetActor()->GetActorLocation()) <= ClickableIntarface->GetDistanceToClick())
			{
				ClickableIntarface->OnClick(this);
			}
		}
	}
	else if(CurrentState == EClickerState::Build)
	{
		//������� ������
		if(GetWorld() && CurrentBuildingArea)
		{
			if(CurrentBuildingArea->CanBuild())
			{
				const auto SpawnTransform = FTransform(CurrentBuildingArea->GetActorLocation());
				const auto NewBuilding = GetWorld()->SpawnActorDeferred<AClickerBuildingBase>(CurrentBuildingArea->GetBuildingClass(), SpawnTransform, this);
				if(NewBuilding)
				{
					OnBuild.Broadcast(CurrentBuildingArea->GetBuildingName());
					UseScore(CurrentBuildingArea->GetBuildPrise());
					NewBuilding->Activate(CurrentBuildingArea->GetBuildingInfo());
					NewBuilding->FinishSpawning(SpawnTransform);
					UGameplayStatics::PlaySound2D(this, BuildSound);
					UGameplayStatics::SpawnEmitterAtLocation(this, BuildParticle, NewBuilding->GetActorLocation());
				}
				CurrentBuildingArea->Destroy();
				CurrentState = EClickerState::Mine;
			}
			else
			{
				UGameplayStatics::PlaySound2D(this, CantBuildSound);
			}
		}
	}
}

void AClickerCharacter::EscapeButton()
{
	if(CurrentState != EClickerState::Build)
	{
		if(const auto ClickerHUD = PlayerControllerCache->GetHUD<AClickerHUD>())
		{
			ClickerHUD->Escape();
			SetEnabledInputs(!ClickerHUD->IsEscapeMenuInViewport());
		}
	}
	else
	{
		if(CurrentBuildingArea)
		{
			CurrentBuildingArea->Destroy();
		}
	}
	CurrentState = EClickerState::Mine;
}

void AClickerCharacter::OpenCloseFactory()
{
	if(CurrentState != EClickerState::Mine) return;

	if(PlayerControllerCache)
	{
		if(const auto ClickerHUD = PlayerControllerCache->GetHUD<AClickerHUD>())
		{
			const auto bIsFactoryWidgetInViewport = ClickerHUD->ToggleFactory();
			SetEnabledInputs(!bIsFactoryWidgetInViewport);
		}
	}
}

void AClickerCharacter::OpenBuildingWidget(UUserWidget* WidgetToOpen)
{
	if(const auto ClickerHUD = PlayerControllerCache->GetHUD<AClickerHUD>())
	{
		ClickerHUD->SetCurrentWidget(WidgetToOpen);
		CurrentState = EClickerState::Update;
		SetEnabledInputs(false);
	}
}

int32 AClickerCharacter::GetDamage() const
{
	return StatsComponent ? StatsComponent->GetDamage() : 1;
}

void AClickerCharacter::Mine(int32 MinedScore)
{
	if(const auto ClickerPlayerState = GetPlayerState<AClickerPlayerState>())
	{
		ClickerPlayerState->AddScore(MinedScore);
	}

	if(!GetMesh()) return;

	if(!GetCurrentMontage())
	{
		PlayAnimMontage(MineAnimation);
	}
}

void AClickerCharacter::AddScore(const int32 ScoreToAdd) const
{
	const auto ClickerPlauerState = GetPlayerState<AClickerPlayerState>();
	if(!ClickerPlauerState) return;

	ClickerPlauerState->AddScore(ScoreToAdd);
}

void AClickerCharacter::UseScore(const int32 ScoreToUse) const
{
	const auto ClickerPlauerState = GetPlayerState<AClickerPlayerState>();
	if(!ClickerPlauerState) return;

	ClickerPlauerState->UseScore(ScoreToUse);
}

void AClickerCharacter::StartBuild(const FBuildingInfo& Info)
{
	if(!GetWorld()) return;
	OpenCloseFactory();
	CurrentState = EClickerState::Build;

	CurrentBuildingArea = GetWorld()->SpawnActorDeferred<AClickerBuildingArea>(BuildingAreaClass, FTransform::Identity);
	if(CurrentBuildingArea)
	{
		CurrentBuildingArea->Init(Info, PlayerControllerCache);
		CurrentBuildingArea->FinishSpawning(FTransform::Identity);
	}
}

void AClickerCharacter::CursorTick(float DeltaSeconds)
{
	if(!PlayerControllerCache) return;

	//������������ ��������� � �������
	RotationToCursor(DeltaSeconds);

	//������������ ������ ��� �������� (������ � ����������� IClicable)
	FHitResult HitResult;
	PlayerControllerCache->GetHitResultUnderCursorByChannel(UEngineTypes::ConvertToTraceType(ECC_Visibility), false, HitResult);
	if(!HitResult.bBlockingHit) return;

	if(IsValid(CurrentObject))
	{
		if(const auto CurrentClicableActor = Cast<IClickable>(CurrentObject))
		{
			CurrentClicableActor->HoverOff();
			CurrentObject = nullptr;
		}
	}
	if(const auto ClicableActor = Cast<IClickable>(HitResult.GetActor()))
	{
		CurrentObject = HitResult.GetActor();
		ClicableActor->HoverOn();
	}
}

void AClickerCharacter::SetEnabledInputs(bool Enabled)
{
	SetActorTickEnabled(Enabled);
	Enabled
		? GetCharacterMovement()->SetMovementMode(MOVE_Walking)
		: GetCharacterMovement()->DisableMovement();
}
