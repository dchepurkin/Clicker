// Created by DChepurkin, All right reserved

#include "Player/ClickerPlayerState.h"

void AClickerPlayerState::BeginPlay()
{
	Super::BeginPlay();
	AddScore(StartScore);
}

void AClickerPlayerState::AddScore(const int32 ScoreToAdd)
{
	SetCurrentScore(CurrentScore + ScoreToAdd);
}

void AClickerPlayerState::UseScore(const int32 ScoreToUse)
{
	SetCurrentScore(CurrentScore - ScoreToUse);
}

void AClickerPlayerState::SetCurrentScore(const int32 NewScore)
{
	CurrentScore = NewScore;
	OnScoreChanged.Broadcast(CurrentScore);
}
