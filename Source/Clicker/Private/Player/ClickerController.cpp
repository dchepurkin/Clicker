// Created by DChepurkin, All right reserved

#include "Player/ClickerController.h"

void AClickerController::BeginPlay()
{
	Super::BeginPlay();

	SetShowMouseCursor(true);
	SetInputMode(FInputModeGameOnly());
}
