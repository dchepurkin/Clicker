// Created by DChepurkin, All right reserved

#include "Game/ClickerGameModeBase.h"

#include "ClickerCharacter.h"
#include "ClickerController.h"
#include "ClickerHUD.h"
#include "ClickerPlayerState.h"

AClickerGameModeBase::AClickerGameModeBase()
{
	DefaultPawnClass = AClickerCharacter::StaticClass();
	PlayerControllerClass = AClickerController::StaticClass();
	PlayerStateClass = AClickerPlayerState::StaticClass();
	HUDClass = AClickerHUD::StaticClass();
}
