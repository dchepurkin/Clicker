// Created by DChepurkin, All right reserved

#include "Game/ClickerHUD.h"

#include "Blueprint/UserWidget.h"

DEFINE_LOG_CATEGORY_STATIC(LogClickerHUD, All, All);

void AClickerHUD::BeginPlay()
{
	Super::BeginPlay();

	GameplayWidget = CreateWidget(GetOwningPlayerController(), GameplayWidgetClass);
	GameplayWidget->AddToViewport();

	FactoryWidget = CreateWidget(GetOwningPlayerController(), FactoryWidgetClass);
	FactoryWidget->SetVisibility(ESlateVisibility::Hidden);
	FactoryWidget->AddToViewport();

	EscapeMenuWidget = CreateWidget(GetOwningPlayerController(), EscapeMenuWidgetClass);
	EscapeMenuWidget->SetVisibility(ESlateVisibility::Hidden);
	EscapeMenuWidget->AddToViewport();
}

bool AClickerHUD::ToggleFactory()
{
	if(!FactoryWidget) return false;

	SetCurrentWidget(FactoryWidget->GetVisibility() == ESlateVisibility::Visible ? nullptr : FactoryWidget);
	return FactoryWidget->GetVisibility() == ESlateVisibility::Visible;
}

void AClickerHUD::Escape()
{
	if(CurrentActiveWidget == EscapeMenuWidget)
	{
		GameplayWidget->SetVisibility(ESlateVisibility::Visible);
		SetCurrentWidget(nullptr);
	}
	else if(CurrentActiveWidget)
	{
		SetCurrentWidget(nullptr);
	}
	else
	{
		GameplayWidget->SetVisibility(ESlateVisibility::Hidden);
		SetCurrentWidget(EscapeMenuWidget);
	}
}

void AClickerHUD::SetCurrentWidget(UUserWidget* NewCurrentWidget)
{
	if(!GetOwningPlayerController()) return;

	if(CurrentActiveWidget)
	{
		CurrentActiveWidget->SetVisibility(ESlateVisibility::Hidden);
	}

	CurrentActiveWidget = NewCurrentWidget;
	if(CurrentActiveWidget)
	{
		CurrentActiveWidget->SetVisibility(ESlateVisibility::Visible);
		GetOwningPlayerController()->SetInputMode(FInputModeGameAndUI());
	}
	else
	{
		GetOwningPlayerController()->SetInputMode(FInputModeGameOnly());
	}
}
