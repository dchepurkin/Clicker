// Created by DChepurkin, All right reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ClickerTypes.h"
#include "ClickerCharacter.generated.h"

class UClickerStatsComponent;
class AClickerBuildingArea;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBuildSignature, const FName, BuildingName);

UCLASS()
class CLICKER_API AClickerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOnBuildSignature OnBuild;
	
	AClickerCharacter();
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	int32 GetDamage() const;
	void Mine(int32 MinedScore);

	void AddScore(const int32 ScoreToAdd) const;
	void UseScore(const int32 ScoreToUse) const;

	UFUNCTION(BlueprintCallable)
	void StartBuild(const FBuildingInfo& Info);	

	UFUNCTION(BlueprintCallable)
	void EscapeButton();

	UFUNCTION(BlueprintCallable)
	void SetEnabledInputs(bool Enabled);

	void OpenCloseFactory();
	void OpenBuildingWidget(UUserWidget* WidgetToOpen);

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	UClickerStatsComponent* StatsComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	float RotationToCursorSpeed = 400.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	UAnimMontage* MineAnimation = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	TSubclassOf<AClickerBuildingArea> BuildingAreaClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	USoundBase* BuildSound = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	USoundBase* CantBuildSound = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	UParticleSystem* BuildParticle = nullptr;

	UPROPERTY()
	APlayerController* PlayerControllerCache = nullptr;

	UPROPERTY()
	AActor* CurrentObject = nullptr;

	UPROPERTY()
	AClickerBuildingArea* CurrentBuildingArea = nullptr;

	EClickerState CurrentState = EClickerState::Mine;

	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void CursorTick(float DeltaSeconds);
	void RotationToCursor(float DeltaSeconds);
	void Click();		
};
