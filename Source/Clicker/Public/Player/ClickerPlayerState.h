// Created by DChepurkin, All right reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "ClickerPlayerState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnScoreChangedSignature, int32, NewScore);

UCLASS()
class CLICKER_API AClickerPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOnScoreChangedSignature OnScoreChanged;

	UFUNCTION(BlueprintCallable)
	bool CanUseScore(const int32 ScoreToUse) const { return ScoreToUse <= CurrentScore; }

	UFUNCTION(BlueprintCallable)
	void UseScore(const int32 ScoreToUse);

	UFUNCTION(BlueprintCallable)
	int32 GetCurrentScore() const { return CurrentScore; }

	void AddScore(const int32 ScoreToAdd);

protected:
	UPROPERTY(EditDefaultsOnly)
	int32 StartScore = 3000;

	virtual void BeginPlay() override;

private:
	int32 CurrentScore = 0;

	void SetCurrentScore(const int32 NewScore);
};
