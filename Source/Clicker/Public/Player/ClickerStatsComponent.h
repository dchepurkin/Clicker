// Created by DChepurkin, All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ClickerStatsComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class CLICKER_API UClickerStatsComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UClickerStatsComponent();
	int32 GetDamage() const;

	UFUNCTION(BlueprintCallable)
	float GetDoubleDamageChance() { return DoubleDamageChance; }

	UFUNCTION(BlueprintCallable)
	void AddDoubleDamageChance(const float AddedChance);

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	int32 BaseDamage = 1;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	float DoubleDamageChance = 0.f;
};
