// Created by DChepurkin, All right reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "ClickerHUD.generated.h"

UCLASS()
class CLICKER_API AClickerHUD : public AHUD
{
	GENERATED_BODY()

public:
	void Escape();
	bool ToggleFactory();
	void SetCurrentWidget(UUserWidget* NewCurrentWidget);
	bool IsEscapeMenuInViewport() const { return CurrentActiveWidget == EscapeMenuWidget; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<UUserWidget> EscapeMenuWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<UUserWidget> GameplayWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<UUserWidget> FactoryWidgetClass;

	virtual void BeginPlay() override;

private:
	UPROPERTY()
	UUserWidget* EscapeMenuWidget = nullptr;

	UPROPERTY()
	UUserWidget* FactoryWidget = nullptr;

	UPROPERTY()
	UUserWidget* GameplayWidget = nullptr;

	UPROPERTY()
	UUserWidget* CurrentActiveWidget = nullptr;
};
