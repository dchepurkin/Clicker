// Created by DChepurkin, All right reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ClickerGameModeBase.generated.h"

UCLASS()
class CLICKER_API AClickerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AClickerGameModeBase();
};
