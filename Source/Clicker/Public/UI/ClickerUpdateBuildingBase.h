// Created by DChepurkin, All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ClickerUpdateBuildingBase.generated.h"

class AClickerBuildingBase;

UCLASS()
class CLICKER_API UClickerUpdateBuildingBase : public UUserWidget
{
	GENERATED_BODY()
public:
	void SetBuilding(AClickerBuildingBase* InBuilding) { Building = InBuilding; }

protected:
	UPROPERTY(BlueprintReadOnly)
	AClickerBuildingBase* Building;
};
