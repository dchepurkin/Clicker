// Created by DChepurkin, All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Clickable.h"
#include "ClickerTypes.h"
#include "GameFramework/Actor.h"
#include "ClickerBuildingBase.generated.h"

class UClickerUpdateBuildingBase;


//������� ����� ������
UCLASS(Abstract)
class CLICKER_API AClickerBuildingBase : public AActor, public IClickable
{
	GENERATED_BODY()

public:
	AClickerBuildingBase();

	
	virtual void Activate(const FBuildingInfo& Info);
	const FBuildingInfo& GetBuildingInfo() const { return BuildingInfo; }

	//IClicable
	virtual void OnClick(AClickerCharacter* ClickedCharacter) override;
	virtual float GetDistanceToClick() const override { return MinDistanceToClick; }
	virtual void HoverOn() override;
	virtual void HoverOff() override;
	//IClickable end

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float MinDistanceToClick = 200.f;	

	virtual void BeginPlay() override;

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	UStaticMeshComponent* StaticMesh;

	UPROPERTY()
	UClickerUpdateBuildingBase* UpdateWidget = nullptr;

	FBuildingInfo BuildingInfo;
};
