// Created by DChepurkin, All right reserved

#pragma once

#include "CoreMinimal.h"
#include "ClickerTypes.h"
#include "GameFramework/Actor.h"
#include "ClickerBuildingArea.generated.h"

class UBoxComponent;

//������ ������� ������ ��������� ��� ������ �������������, ���������� ���������� ���������� ����� � ����������� ������������� � ������ �����
UCLASS(Abstract)
class CLICKER_API AClickerBuildingArea : public AActor
{
	GENERATED_BODY()

public:
	AClickerBuildingArea();
	virtual void Tick(float DeltaTime) override;

	//������������� BuildingInfo � OwningController
	void Init(const FBuildingInfo& Info, APlayerController* Controller);
	
	const TSubclassOf<AClickerBuildingBase>& GetBuildingClass() const { return BuildingInfo.Class; }
	bool CanBuild() const { return bCanBuild; }
	int32 GetBuildPrise() const { return BuildingInfo.Prise; }
	const FBuildingInfo& GetBuildingInfo() const { return BuildingInfo; }
	const FName& GetBuildingName() const { return BuildingInfo.Name; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UBoxComponent* Collision;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UMaterial* DecalMaterial;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TMap<bool, FLinearColor> CanBuildColor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName ColorParameterName = "Color";

	virtual void BeginPlay() override;

private:
	UPROPERTY()
	UMaterialInstanceDynamic* DecalMaterialInstance;

	UPROPERTY()
	UDecalComponent* Decal;

	UPROPERTY()
	APlayerController* OwningController;

	bool bCanBuild = true;

	FBuildingInfo BuildingInfo;

	UFUNCTION()
	void BeginOverlapCallback(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
	                          bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void EndOverlapCallback(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void UpdateColor();
};
