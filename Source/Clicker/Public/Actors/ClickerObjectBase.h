// Created by DChepurkin, All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Clickable.h"
#include "GameFramework/Actor.h"
#include "ClickerObjectBase.generated.h"

class UBoxComponent;
class UWidgetComponent;

DECLARE_MULTICAST_DELEGATE(FOnDestroySignature);

//������� ����� ������� ��� ������ �����
UCLASS(Abstract)
class CLICKER_API AClickerObjectBase : public AActor, public IClickable
{
	GENERATED_BODY()

public:
	FOnDestroySignature OnDestroy;

	AClickerObjectBase();

	UFUNCTION(BlueprintCallable)
	float GetScorePercent() const { return static_cast<float>(CurrentScore) / MaxScore; }

	//IClicable
	virtual void OnClick(AClickerCharacter* ClickedCharacter) override;
	virtual float GetDistanceToClick() const override { return MinDistanceToClick; }
	virtual void HoverOn() override;
	virtual void HoverOff() override;
	//IClicable end

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Clicker|Score")
	int32 MaxScore = 10;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Clicker")
	float MinDistanceToClick = 200.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Clicker|FX")
	USoundBase* ClickSound = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Clicker|FX")
	UParticleSystem* ClickParticle = nullptr;

	//��� ������ ������� ������ ��������� ����� ������������ ���������� ����
	UFUNCTION(BlueprintImplementableEvent)
	void DamageEvent(int32 Damage);

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	UBoxComponent* Collision;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	UWidgetComponent* ScoreProgressBar;

	int32 CurrentScore = 0;
};
