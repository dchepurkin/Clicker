// Created by DChepurkin, All right reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ClickerObjectSpawner.generated.h"

class UBoxComponent;
class AClickerObjectBase;

//������� �������� ��� ����� �����
UCLASS()
class CLICKER_API AClickerObjectSpawner : public AActor
{
	GENERATED_BODY()

public:
	AClickerObjectSpawner();

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	UBoxComponent* SpawnArea;

	UPROPERTY(EditAnywhere, meta=(AllowPrivateAccess = true))
	TArray<TSubclassOf<AClickerObjectBase>> ObjectsClasses;

	UPROPERTY(EditAnywhere, meta=(AllowPrivateAccess = true))
	int32 MaxObjectsInScene = 10;

	UPROPERTY(EditAnywhere, meta=(AllowPrivateAccess = true))
	float SpawnRate = 5.f;

	FTimerHandle SpawnTimerHandle;
	int32 CurrentObjectsCount = 0;

	void SpawnTimerCallback();
	const TSubclassOf<AClickerObjectBase>& GetRandomObjectClass() const;

	void OnObjectDestroyCallback();
};
