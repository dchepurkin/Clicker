// Created by DChepurkin, All right reserved

#pragma once

#include "CoreMinimal.h"
#include "Actors/ClickerBuildingBase.h"
#include "ClickerScoreFarmBuilding.generated.h"

//������ ������� ������ - ��� ������, ����������� N-�� ���������� ����� � ����������� �� ������ ���������
UCLASS(Abstract)
class CLICKER_API AClickerScoreFarmBuilding : public AClickerBuildingBase
{
	GENERATED_BODY()

public:
	virtual void Activate(const FBuildingInfo& Info) override;

	UFUNCTION(BlueprintCallable)
	int32 GetScorePerTick() const { return ScorePerTick; }

	UFUNCTION(BlueprintCallable)
	void UpdateScorePerTick();

	UFUNCTION(BlueprintCallable)
	bool CanUpdateScore() const { return ScorePerTickCurrentUpdate < ScorePerTickMaxUpdate; }

	UFUNCTION(BlueprintCallable)
	int32 GetScoreUpdatePrise() const;

	UFUNCTION(BlueprintCallable)
	float GetFarmRate() const { return 1.0 / FarmRate; }

	UFUNCTION(BlueprintCallable)
	void UpdateFarmRate();

	UFUNCTION(BlueprintCallable)
	bool CanUpdateFarmRate() const { return FarmRateCurrentUpdate < FarmRateMaxUpdate; }

	UFUNCTION(BlueprintCallable)
	int32 GetFarmRateUpdatePrise() const;

private:
	//���-�� ����� ����������� �� ���� ��� �������
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	int32 ScorePerTick = 1;

	//�� ������� ������������� ScorePerTick ��� ���������
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	int32 ScorePerTickUpdateCoef = 1;

	//������������ ������� ��������� ���������� �����
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	int32 ScorePerTickMaxUpdate = 10;

	//������� ���������� ���������� � ��������� ��������� ���������� ���������� �����
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	UDataTable* ScoreUpdateInfoDataTable;

	//�������� ��������� �����
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true, ClampMin = 0.01))
	float FarmRate = 5.f;

	//�� ������� ����������� FarmRate ��� ���������
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	float FarmRateUpdateCoef = 0.1f;

	//������������ ������� ��������� FarmRate
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	int32 FarmRateMaxUpdate = 6;

	//������� ���������� ���������� � ��������� ��������� ��������� ��������� �����
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	UDataTable* FarmRateUpdateInfoDataTable;

	//������� ������� ���������
	int32 ScorePerTickCurrentUpdate = 1;
	int32 FarmRateCurrentUpdate = 1;

	FTimerHandle FarmTimerHandle;

	void FarmTimerCallback() const;
};
