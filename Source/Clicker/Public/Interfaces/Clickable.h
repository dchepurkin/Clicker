// Created by DChepurkin, All right reserved

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Clickable.generated.h"

class AClickerCharacter;

UINTERFACE(MinimalAPI)
class UClickable : public UInterface
{
	GENERATED_BODY()
};

class CLICKER_API IClickable
{
	GENERATED_BODY()

public:
	virtual void OnClick(AClickerCharacter* ClickedCharacter) = 0;
	virtual float GetDistanceToClick() const = 0;
	virtual void HoverOn() = 0;
	virtual void HoverOff() = 0;
};
