#pragma once
#include "Engine/DataTable.h"

#include "ClickerTypes.generated.h"

UENUM(BlueprintType)
enum class EClickerState : uint8
{
	Mine,
	Build,
	Update
};

USTRUCT(BlueprintType)
struct FBuildingInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName Name = NAME_None;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class AClickerBuildingBase> Class = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Decription = FText();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Prise = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector2D Size = FVector2D::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UUserWidget> UpdateWidgetClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsCreated = false;
};

USTRUCT(BlueprintType)
struct FUpdatePriseInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 UpdateLevel = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 UpdatePrise = 100;
};
