// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Clicker : ModuleRules
{
	public Clicker(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "UMG"});

		PrivateDependencyModuleNames.AddRange(new string[] { });

		PublicIncludePaths.AddRange(new string[]
		{
			"Clicker/",
			"Clicker/Public/",
			"Clicker/Public/UI",
			"Clicker/Public/Game",
			"Clicker/Public/Actors",
			"Clicker/Public/Interfaces",
			"Clicker/Public/Player",
		});

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}